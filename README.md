# mouse-sci-omics

This repository contains the scripts for the analysis of single-cell combinatorial (sci) Hi-C, sci-RNA-seq, and sci-ATAC-seq data related to the article below. Based on trajectory analyses, we show that three distinct nuclear structure states are detected reflecting discrete and profound simultaneous changes not only to the structure of the X chromosomes, but also to that of autosomes during differentiation. Our study reveals that long-range structural changes to chromosomes appear as discrete events, unlike progressive changes in gene expression and chromatin accessibility. 

Giancarlo Bonora, Vijay Ramani, Ritambhara Singh, He Fang, Dana L. Jackson, Sanjay Srivatsan, Ruolan Qiu, Choli Lee, Cole Trapnell, Jay Shendure, Zhijun Duan, Xinxian Deng, William S. Noble, Christine M. Disteche. Single-cell landscape of nuclear configuration and gene expression during stem cell differentiation and X inactivation. Genome Biology. (2021) doi: https://doi.org/10.1101/2020.11.20.390765 

All scripts are provided as is without warranty and at the user's own risk. This is not a software package release, but rather the provision of code to facilitate the reproduction our analyses. Future support of these scripts is not guaranteed.
